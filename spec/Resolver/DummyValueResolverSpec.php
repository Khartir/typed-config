<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Resolver;

use Khartir\TypedConfig\Resolver\ScalarTypeResolver;
use PhpSpec\ObjectBehavior;

class DummyValueResolverSpec extends ObjectBehavior
{
    public function it_resolves_to_default_values(\ReflectionParameter $parameter): void
    {
        $defaults = [
            'int' => 0,
            'string' => '',
            'float' => .0,
            'array' => [],
            'bool' => false,
        ];
        $this->beConstructedWith([], true, true, $defaults);
        foreach ($defaults as $type => $value) {
            $parameter->allowsNull()->willReturn(false);
            $parameter->getType()->willReturn($type);
            $parameter->isDefaultValueAvailable()->willReturn(false);
            $this->canResolve($parameter)
                ->shouldBe(true);
            $this->resolve($parameter, [])
                ->shouldBe($value);
        }
    }

    public function it_resolves_to_null_if_nullable(\ReflectionParameter $parameter): void
    {
        $this->beConstructedWith([], true, true, []);
        $parameter->getType()->willReturn('bool');
        $parameter->allowsNull()->willReturn(true);
        $parameter->isDefaultValueAvailable()->willReturn(false);
        $this->resolve($parameter, [])
            ->shouldBe(null);
    }

    public function it_does_not_resolve_to_null_if_configured(\ReflectionParameter $parameter): void
    {
        $this->beConstructedWith([], true, false, ['bool' => false]);
        $parameter->getType()->willReturn('bool');
        $parameter->allowsNull()->willReturn(true);
        $parameter->isDefaultValueAvailable()->willReturn(false);
        $this->resolve($parameter, [])
            ->shouldBe(false);
    }

    public function it_resolves_to_default_value(\ReflectionParameter $parameter): void
    {
        $this->beConstructedWith([], true, true, []);
        $parameter->getType()->willReturn('string');
        $parameter->allowsNull()->willReturn(false);
        $parameter->isDefaultValueAvailable()->willReturn(true);
        $parameter->getDefaultValue()->willReturn('dummy');
        $this->resolve($parameter, [])
            ->shouldBe('dummy');
    }

    public function it_does_not_resolves_to_default_value_if_configured(\ReflectionParameter $parameter): void
    {
        $this->beConstructedWith([], false, true, ['string' => '']);
        $parameter->getType()->willReturn('string');
        $parameter->allowsNull()->willReturn(false);
        $parameter->isDefaultValueAvailable()->willReturn(true);
        $parameter->getDefaultValue()->willReturn('dummy');
        $this->resolve($parameter, [])
            ->shouldBe('');
    }

    public function it_resolves_custom_types(\ReflectionParameter $parameter): void
    {
        $value = new \DateTime();
        $this->beConstructedWith([], true, true, [\DateTime::class => $value]);
        $parameter->allowsNull()->willReturn(false);
        $parameter->isDefaultValueAvailable()->willReturn(false);
        $parameter->getType()->willReturn(\DateTime::class);
        $this->canResolve($parameter)->shouldBe(true);
        $this->resolve($parameter, [])
            ->shouldBe($value);
    }

    public function it_resolves_given_values(\ReflectionParameter $parameter): void
    {
        $this->beConstructedWith([new ScalarTypeResolver()], true, true, []);
        $parameter->getType()->willReturn('string');
        $parameter->allowsNull()->willReturn(false);
        $parameter->isDefaultValueAvailable()->willReturn(false);
        $this->resolve($parameter, ['a', 'b', 'c'])->shouldBe('c');
    }
}
