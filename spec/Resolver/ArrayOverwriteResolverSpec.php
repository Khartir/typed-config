<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Resolver;

use Khartir\TypedConfig\Exception\InvalidArgumentException;
use PhpSpec\ObjectBehavior;

class ArrayOverwriteResolverSpec extends ObjectBehavior
{
    public function it_can_resolve_arrays(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('array');
        $this->canResolve($parameter)->shouldBe(true);

        $parameter->getType()->willReturn('anything');
        $this->canResolve($parameter)->shouldBe(false);

        $parameter->getType()->willReturn(null);
        $this->canResolve($parameter)->shouldBe(false);
    }

    public function it_resolves_to_last_value(\ReflectionParameter $parameter): void
    {
        $parameter->allowsNull()->willReturn(true);
        $this->resolve($parameter, [['a'], ['b'], ['c']])->shouldBe(['c']);
        $this->resolve($parameter, [[['foo' => 'bar']], [['a' => 'b']]])
            ->shouldBe([['a' => 'b']]);
    }

    public function it_returns_null_if_allowed(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('array');
        $parameter->getName()->willReturn('param');
        $parameter->allowsNull()->willReturn(true);
        $this->resolve($parameter, [null, null])->shouldBe(null);
    }

    public function it_throws_on_invalid_value(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('array');
        $parameter->allowsNull()->willReturn(false);
        $parameter->getName()->willReturn('param');
        $this->shouldThrow(
            new InvalidArgumentException('Invalid value 1, expected value of type array for "param" at path "/".')
        )->duringResolve($parameter, [1]);
    }

    public function it_throws_on_invalid_nullable_value(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('array');
        $parameter->allowsNull()->willReturn(true);
        $parameter->getName()->willReturn('param');
        $this->shouldThrow(
            new InvalidArgumentException(
                'Invalid value 1, expected value of type array or NULL for "param" at path "/".'
            )
        )->duringResolve($parameter, [1]);
    }
}
