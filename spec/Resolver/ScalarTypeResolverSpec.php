<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Resolver;

use Khartir\TypedConfig\Exception\InvalidArgumentException;
use PhpSpec\ObjectBehavior;

class ScalarTypeResolverSpec extends ObjectBehavior
{
    public function it_can_handle_scalar_types(\ReflectionParameter $parameter): void
    {
        $validTypes = ['int', 'string', 'bool', 'float'];
        foreach ($validTypes as $type) {
            $parameter->getType()->willReturn($type);
            $this->canResolve($parameter)->shouldBe(true);
        }
        $invalidTypes = [null, 'resource', \stdClass::class];
        foreach ($invalidTypes as $type) {
            $parameter->getType()->willReturn($type);
            $this->canResolve($parameter)->shouldBe(false);
        }
    }

    public function it_returns_last_value(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('string');
        $this->resolve($parameter, ['a', 'b', 'c'])->shouldBe('c');
    }

    public function it_returns_null_if_allowed(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('string');
        $parameter->allowsNull()->willReturn(true);
        $this->resolve($parameter, [null])->shouldBe(null);
    }

    public function it_throws_on_invalid_value(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('string');
        $parameter->getName()->willReturn('param');
        $parameter->allowsNull()->willReturn(true);
        $this->shouldThrow(
            new InvalidArgumentException(
                'Invalid value 1, expected value of type string or NULL for "param" at path "/".'
            )
        )->duringResolve($parameter, [1]);
    }

    public function it_throws_on_null_value_if_not_nullable(\ReflectionParameter $parameter): void
    {
        $parameter->getType()->willReturn('string');
        $parameter->allowsNull()->willReturn(false);
        $parameter->getName()->willReturn('param');
        $this->shouldThrow(
            new InvalidArgumentException('Invalid value NULL, expected value of type string for "param" at path "/".')
        )->duringResolve($parameter, [null]);
    }
}
