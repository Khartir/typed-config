<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Extractor;

use Khartir\TypedConfig\Extractor\ExactMatchExtractor;
use PhpSpec\ObjectBehavior;

class DummyValueExtractorSpec extends ObjectBehavior
{
    public function it_returns_empty_array_if_no_values_given(): void
    {
        $this->findValues('anything', [])
            ->shouldBeEqualTo([]);
    }

    public function it_returns_matching_values_if_found(): void
    {
        $this->beConstructedWith(new ExactMatchExtractor());
        $this->findValues('key', [['key' => 'value']])
            ->shouldBeEqualTo(['value']);
    }

    public function it_returns_multiple_matching_values_if_found(): void
    {
        $this->beConstructedWith(new ExactMatchExtractor());
        $this->findValues('key', [
            ['key' => ['value','bar']],
            ['key' => ['foo']],
        ])
            ->shouldBeEqualTo([['value', 'bar'], ['foo']]);
    }

    public function it_returns_optional_matching_values_if_found(): void
    {
        $this->beConstructedWith(new ExactMatchExtractor());
        $this->findOptionalValues('key', [['key' => 'value']])
            ->shouldBeEqualTo(['value']);
    }
}
