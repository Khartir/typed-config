<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Extractor;

use PhpSpec\ObjectBehavior;

class SnakeCaseExtractorSpec extends ObjectBehavior
{
    public function it_can_find_snaked_case_values(): void
    {
        foreach ($this->getTestCases() as [$key, $arrayKey]) {
            $value = random_bytes(5);
            $value2 = random_int(10, 1000);
            $this->findValues($key, [
                [
                    $arrayKey => ['foo' => $value],
                ], [
                    $arrayKey => ['count' => $value2],
                ]])->shouldBeEqualTo([['foo' => $value], ['count' => $value2]]);
        }
    }

    /**
     * @see https://stackoverflow.com/questions/1993721/how-to-convert-pascalcase-to-pascal-case/35719689#35719689
     * @return array<array<string, string>>
     */
    private function getTestCases(): array
    {
        return [
            [
                'someValue',
                'some_value',
            ],
            [
                'easy',
                'easy',
            ],
            [
                'HTML',
                'html',
            ],
            [
                'simpleXML',
                'simple_xml',
            ],
            [
                'PDFLoad',
                'pdf_load',
            ],
            [
                'startMIDDLELast',
                'start_middle_last',
            ],
            [
                'AString',
                'a_string',
            ],
            [
                'Some4Numbers234',
                'some4_numbers234',
            ],
            [
                'TEST123String',
                'test123_string',
            ],
            [
                'some_value',
                'some_value',
            ],
            [
                'some__value',
                'some__value',
            ],
            [
                '_some_value_',
                '_some_value_',
            ],
            [
                'some_Value',
                'some_value',
            ],
            [
                'SomeValue',
                'some_value',
            ],
            [
                'SomeValueFoo',
                'some_value_foo',
            ],
            [
                'some-value',
                'some-value',
            ],
            [
                'some+value',
                'some+value',
            ],
            [
                'aBaBaB',
                'a_ba_ba_b',
            ],
            [
                'BaBaBa',
                'ba_ba_ba',
            ],
            [
                'libC',
                'lib_c',
            ],
        ];
    }
}
