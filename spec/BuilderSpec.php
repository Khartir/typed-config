<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig;

use Khartir\TypedConfig\Exception\InvalidArgumentException;
use Khartir\TypedConfig\Exception\InvalidPropertyException;
use Khartir\TypedConfig\Exception\InvalidRootClassException;
use Khartir\TypedConfig\Exception\MissingValueException;
use Khartir\TypedConfig\Extractor\SnakeCaseExtractor;
use Khartir\TypedConfig\Resolver\ArrayOverwriteResolver;
use Khartir\TypedConfig\Resolver\ResolverInterface;
use PhpSpec\Exception\Example\SkippingException;
use spec\Khartir\TypedConfig\Stubs\AllScalarConfig;
use spec\Khartir\TypedConfig\Stubs\ArrayConfig;
use spec\Khartir\TypedConfig\Stubs\CaseConfig;
use spec\Khartir\TypedConfig\Stubs\DeeplyNestedConfig;
use spec\Khartir\TypedConfig\Stubs\DefaultValueConfig;
use spec\Khartir\TypedConfig\Stubs\InvalidTypeConfig;
use spec\Khartir\TypedConfig\Stubs\MissingNestedClassConfig;
use spec\Khartir\TypedConfig\Stubs\MissingTypeConfig;
use spec\Khartir\TypedConfig\Stubs\NestedArrayConfig;
use spec\Khartir\TypedConfig\Stubs\NestedConfig;
use spec\Khartir\TypedConfig\Stubs\NestedMissingTypeConfig;
use spec\Khartir\TypedConfig\Stubs\NestedUnconstructableConfig;
use spec\Khartir\TypedConfig\Stubs\NestedValidatingConfig;
use spec\Khartir\TypedConfig\Stubs\OptionalNested;
use spec\Khartir\TypedConfig\Stubs\SimpleConfig;
use PhpSpec\ObjectBehavior;
use spec\Khartir\TypedConfig\Stubs\UnconfiguredNestedConfig;
use spec\Khartir\TypedConfig\Stubs\UnconstructableConfig;
use spec\Khartir\TypedConfig\Stubs\ValidatingConfig;

class BuilderSpec extends ObjectBehavior
{
    public function it_validates_resolvers_in_constructor(): void
    {
        $this->beConstructedWith(null, [new \stdClass()]);
        $this->shouldThrow(new \InvalidArgumentException(
            'Resolver must implement ' . ResolverInterface::class
        ))
            ->duringInstantiation();
    }

    public function it_instantiates_a_simple_object(): void
    {
        $this->build(SimpleConfig::class, [
            ['foo' => 'bar', 'count' => 3],
        ])->shouldBeLike(new SimpleConfig('bar', 3));
    }

    public function it_instantiates_a_simple_object_regardless_of_argument_order(): void
    {
        $this->build(SimpleConfig::class, [
            ['count' => 3, 'foo' => 'bar'],
        ])->shouldBeLike(new SimpleConfig('bar', 3));
    }

    public function it_instantiates_an_object_with_a_nested_object(): void
    {
        $this->build(NestedConfig::class, [
            ['test' => true, 'simple' => ['count' => 3, 'foo' => 'bar']],
        ])->shouldBeLike(new NestedConfig(new SimpleConfig('bar', 3), true));
    }

    public function it_instantiates_a_simple_object_from_multiple_arrays(): void
    {
        $this->build(SimpleConfig::class, [
            ['foo' => 'bar'],
            ['count' => 3],
        ])->shouldBeLike(new SimpleConfig('bar', 3));
    }

    public function it_instantiates_an_object_with_a_nested_object_from_multiple_arrays(): void
    {
        $this->build(NestedConfig::class, [
            ['test' => true],
            ['simple' => ['count' => 3]],
            ['simple' => ['foo' => 'bar']],
        ])->shouldBeLike(new NestedConfig(new SimpleConfig('bar', 3), true));
    }

    public function it_can_handle_all_scalar_types(): void
    {
        $this->build(AllScalarConfig::class, [
            [
                'foo' => 'bar',
                'count' => 9,
                'test' => false,
                'percentage' => 0.01,
            ],
        ])->shouldBeLike(new AllScalarConfig('bar', 9, false, 0.01));
    }

    public function it_uses_default_values_if_no_value_is_provided(): void
    {
        $this->build(DefaultValueConfig::class, [])
            ->shouldBeLike(new DefaultValueConfig());
    }

    public function it_uses_default_values_if_only_some_values_are_provided(): void
    {
        $this->build(DefaultValueConfig::class, [
            ['count' => 1],
        ])->shouldBeLike(new DefaultValueConfig('default', 1));

        $this->build(DefaultValueConfig::class, [
            ['foo' => 'bar'],
        ])->shouldBeLike(new DefaultValueConfig('bar'));
    }

    public function it_can_handle_nullable_types(): void
    {
        $this->build(DefaultValueConfig::class, [
            ['nullable' => null],
        ])->shouldBeLike(new DefaultValueConfig());
    }

    public function it_is_typesafe(): void
    {
        $this->shouldThrow(
            new InvalidArgumentException('Invalid value "3", expected value of type int for "count" at path "/".')
        )
        ->duringBuild(SimpleConfig::class, [
            ['count' => '3', 'foo' => 'bar'],
        ]);

        $e = InvalidArgumentException::create('Invalid value "3", expected value of type int', 'count');
        $e->addPath('simple');
        $this->shouldThrow($e)->duringBuild(NestedConfig::class, [
                ['test' => true, 'simple' => ['count' => '3', 'foo' => 'bar']],
            ]);
    }

    public function it_has_proper_error_messages(): void
    {
        $this->shouldThrow(
            new InvalidArgumentException('Invalid value NULL, expected value of type int for "count" at path "/".')
        )
            ->duringBuild(SimpleConfig::class, [
                ['count' => null, 'foo' => 'bar'],
            ]);

        $this->shouldThrow(
            new InvalidArgumentException('Invalid value 3, expected value of type string for "foo" at path "/".')
        )
            ->duringBuild(SimpleConfig::class, [
                ['count' => 3, 'foo' => 3],
            ]);

        $this->shouldThrow(
            new InvalidArgumentException('Invalid value 3.08, expected value of type string for "foo" at path "/".')
        )
            ->duringBuild(SimpleConfig::class, [
                ['count' => 3, 'foo' => 3.08],
            ]);

        $this->shouldThrow(
            new InvalidArgumentException('Invalid value TRUE, expected value of type string for "foo" at path "/".')
        )
            ->duringBuild(SimpleConfig::class, [
                ['count' => 3, 'foo' => true],
            ]);

        $this->shouldThrow(
            new InvalidArgumentException('Invalid value FALSE, expected value of type string for "foo" at path "/".')
        )
            ->duringBuild(SimpleConfig::class, [
                ['count' => 3, 'foo' => false],
            ]);

        $this->shouldThrow(
            new InvalidArgumentException('Invalid value "", expected value of type int for "count" at path "/".')
        )
            ->duringBuild(SimpleConfig::class, [
                ['count' => '', 'foo' => 'bar'],
            ]);

        $this->shouldThrow(
            new InvalidArgumentException(
                'Invalid value of type "array", expected value of type int for "count" at path "/".'
            )
        )
            ->duringBuild(SimpleConfig::class, [
                ['count' => [], 'foo' => 'bar'],
            ]);
    }

    public function it_uses_last_given_value(): void
    {
        $this->build(SimpleConfig::class, [
            ['foo' => 'bar', 'count' => 3],
            ['foo' => 'new'],
        ])->shouldBeLike(new SimpleConfig('new', 3));

        $this->build(NestedConfig::class, [
            ['test' => true, 'simple' => ['count' => 3, 'foo' => 'bar']],
            ['test' => false, 'simple' => ['count' => 5]],
            ['simple' => ['foo' => 'new']],
        ])->shouldBeLike(new NestedConfig(new SimpleConfig('new', 5), false));
    }

    public function it_reports_missing_values(): void
    {
        $this->shouldThrow(new MissingValueException('Value for "foo" at path "/" not found.'))
            ->duringBuild(SimpleConfig::class, [
                ['count' => '3'],
            ]);

        $this->shouldThrow(new MissingValueException('Value for "simple" at path "/" not found.'))
            ->duringBuild(NestedConfig::class, [
                ['count' => '3'],
            ]);

        $expected = new MissingValueException();
        $expected->setName('foo');
        $expected->addPath('simple');
        $this->shouldThrow($expected)
            ->duringBuild(NestedConfig::class, [
                ['simple' => []],
            ]);

        $expected = new MissingValueException();
        $expected->setName('values');
        $expected->addPath('nested');
        $this->shouldThrow($expected)
            ->duringBuild(NestedArrayConfig::class, [
                ['nested' => []],
            ]);
    }

    public function it_can_handle_array_parameters(): void
    {
        $this->build(ArrayConfig::class, [
            ['values' => ['foo' => 'bar'],],
            ['values' => ['count' => 5],],
        ])->shouldBeLike(new ArrayConfig(['foo' => 'bar',
            'count' =>5]));
    }

    public function it_can_handle_snake_case(): void
    {
        $this->beConstructedWith(new SnakeCaseExtractor());
        $this->build(CaseConfig::class, [
            ['foo_bar' => 'baz',],
        ])->shouldBeLike(new CaseConfig('baz'));
    }

    public function it_reports_missing_classes(): void
    {
        $this->shouldThrow(new InvalidRootClassException('Root-class "UnknownClass" not found.'))
            ->duringBuild('UnknownClass', []);
        $this->shouldThrow(new InvalidPropertyException(
            'Class "spec\Khartir\TypedConfig\Stubs\UnknownClass" not found for "test" at path "/".'
        ))
            ->duringBuild(MissingNestedClassConfig::class, []);
    }

    public function it_handles_classes_without_constructor(): void
    {
        $this->shouldThrow(new InvalidRootClassException(
            'Root-class "stdClass" does not have a constructor.'
        ))
            ->duringBuild(\stdClass::class, []);
        $this->build(UnconfiguredNestedConfig::class, [['test' => null]])
            ->shouldBeLike(new UnconfiguredNestedConfig(new \stdClass()));
    }

    public function it_reports_classes_with_private_constructor(): void
    {
        $this->shouldThrow(new InvalidRootClassException(
            'Root-class "spec\Khartir\TypedConfig\Stubs\UnconstructableConfig" does not have a public constructor.'
        ))
            ->duringBuild(UnconstructableConfig::class, []);

        $this->shouldThrow(new InvalidPropertyException(
            //phpcs:ignore
            'Class "spec\Khartir\TypedConfig\Stubs\UnconstructableConfig" does not have a public constructor for "test" at path "/".'
        ))
            ->duringBuild(NestedUnconstructableConfig::class, [['test' => []]]);
    }

    public function it_reports_untyped_parameters(): void
    {
        $this->shouldThrow(new InvalidPropertyException(
            'Missing parameter type for "test" at path "/".'
        ))
            ->duringBuild(MissingTypeConfig::class, [['test' => []]]);

        $exception = InvalidPropertyException::create('Missing parameter type', 'test');
        $exception->addPath('nested');
        $this->shouldThrow($exception)
            ->duringBuild(NestedMissingTypeConfig::class, [['nested' => ['test' => []]]]);
    }

    public function it_reports_incorretly_typed_parameters(): void
    {
        $this->shouldThrow(new InvalidPropertyException(
            'Invalid type object for "test" at path "/".'
        ))
            ->duringBuild(InvalidTypeConfig::class, [['test' => []]]);
    }

    public function it_handles_validation_in_constructors(): void
    {
        $this->shouldThrow(new InvalidArgumentException(
            'Invalid Argument for "test" at path "/".'
        ))
            ->duringBuild(ValidatingConfig::class, [['test' => '']]);

        $exception = InvalidArgumentException::create('Invalid Argument', 'test');
        $exception->addPath('nested');
        $this->shouldThrow($exception)
            ->duringBuild(NestedValidatingConfig::class, [['nested' => ['test' => '']]]);
    }

    public function it_handles_nesting_on_multiple_levels(): void
    {
        $expected = new MissingValueException();
        $expected->setName('foo');
        $expected->addPath('simple');
        $expected->addPath('nestedConfig');
        $this->shouldThrow($expected)
            ->duringBuild(DeeplyNestedConfig::class, [
                ['nestedConfig' => ['simple' => []]],
            ]);

        $expected = InvalidPropertyException::create(
            //phpcs:ignore
            'Class "spec\Khartir\TypedConfig\Stubs\UnconstructableConfig" does not have a public constructor',
            'test'
        );
        $expected->addPath('nested');

        $this->shouldThrow($expected)
            ->duringBuild(
                DeeplyNestedConfig::class,
                [
                [
                    'nestedConfig' => [
                        'simple' => ['foo' => 'bar', 'count' => 1],
                        'test' => true,
                    ],
                    'nested' => ['test' => ''],
                ],
                ]
            );
    }

    public function it_allows_override_of_resolvers(): void
    {
        $this->beConstructedWith(null, [new ArrayOverwriteResolver()]);
        $this->build(
            ArrayConfig::class,
            [
                ['values' => ['foo' => 'bar'],],
                ['values' => ['count' => 5],],
            ]
        )->shouldBeLike(new ArrayConfig(['count' => 5]));
    }

    public function it_uses_attributes_to_determine_resolvers(): void
    {
        if (version_compare(PHP_VERSION, '8.0', '<')) {
            throw new SkippingException('Requires PHP 8');
        }
        $this->build(
            \spec\Khartir\TypedConfig\Stubs\Attribute\ArrayConfig::class,
            [
            ['values' => ['foo' => 'bar'],],
            ['values' => ['count' => 5],],
            ]
        )->shouldBeLike(new \spec\Khartir\TypedConfig\Stubs\Attribute\ArrayConfig(['count' => 5]));
    }

    public function it_uses_attributes_in_nested_objects(): void
    {
        if (version_compare(PHP_VERSION, '8.0', '<')) {
            throw new SkippingException('Requires PHP 8');
        }
        $this->build(
            \spec\Khartir\TypedConfig\Stubs\Attribute\NestedConfig::class,
            [
            ['test' => true],
            [
                'child' => ['values' => ['foo' => 'bar'],],
            ],
            [
                'child' => ['values' => ['count' => 5],],
            ],
            ]
        )->shouldBeLike(
            new \spec\Khartir\TypedConfig\Stubs\Attribute\NestedConfig(
                new \spec\Khartir\TypedConfig\Stubs\Attribute\ArrayConfig(['count' => 5]),
                true
            )
        );
    }

    public function it_uses_annotations_to_determine_resolvers(): void
    {
        $this->enableAnnotations();
        $this->build(
            \spec\Khartir\TypedConfig\Stubs\Annotation\ArrayConfig::class,
            [
                ['values' => ['foo' => 'bar'],],
                ['values' => ['count' => 5],],
            ]
        )->shouldBeLike(new \spec\Khartir\TypedConfig\Stubs\Annotation\ArrayConfig(['count' => 5]));
    }

    public function it_uses_default_without_annotations(): void
    {
        $this->enableAnnotations();
        $this->build(
            ArrayConfig::class,
            [
                ['values' => ['foo' => 'bar'],],
                ['values' => ['count' => 5],],
            ]
        )->shouldBeLike(new ArrayConfig(['foo' => 'bar', 'count' => 5]));
    }

    public function it_throws_on_invalid_data(): void
    {
        $this->shouldThrow(new InvalidArgumentException(
            'List of arrays expected, string given at key foo for "root-element" at path "/".'
        ))->duringBuild(SimpleConfig::class, [
                'foo' => 'bar',
            ]);
        $this->shouldThrow(new InvalidArgumentException(
            'List of arrays expected, string given at key 0 for "simple" at path "/".'
        ))->duringBuild(NestedConfig::class, [
                ['simple' => 'foo', 'test' => false],
            ]);
    }

    public function it_allows_optional_nested_configs(): void
    {
        $this->build(
            OptionalNested::class,
            [
                ['foo' => 'bar'],
            ]
        )->shouldBeLike(new OptionalNested('bar'));
    }

    public function it_allows_optional_nested_configs_with_later_parameters_set(): void
    {
        $this->build(
            OptionalNested::class,
            [
                ['foo' => 'bar'],
                ['otherNested' => ['foo' => '', 'count' => 5]],
            ]
        )->shouldBeLike(new OptionalNested('bar', null, new SimpleConfig('', 5)));
    }
}
