<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Annotation;

use PhpSpec\ObjectBehavior;

class ResolveSpec extends ObjectBehavior
{
    public function it_validates_resolver_names(): void
    {
        $this->beConstructedWith(['value' => ['test' => 'dummy']]);
        $this->shouldThrow(new \InvalidArgumentException(
            'Resolver must implement Khartir\TypedConfig\Resolver\ResolverInterface, dummy given'
        ))
            ->duringInstantiation();
    }
}
