<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Exception;

use Khartir\TypedConfig\Exception\AbstractException;
use PhpSpec\ObjectBehavior;

class MissingValueExceptionSpec extends ObjectBehavior
{
    public function it_extends_base_exception(): void
    {
        $this->shouldHaveType(AbstractException::class);
    }

    public function it_adds_name_to_message(): void
    {
        $this->setName('dummy');
        $this->getMessage()->shouldBeEqualTo('Value for "dummy" at path "/" not found.');
    }

    public function it_adds_path_to_message(): void
    {
        $this->setName('dummy');
        $this->addPath('level1');
        $this->getMessage()->shouldBeEqualTo('Value for "dummy" at path "/level1" not found.');
        $this->addPath('level2');
        $this->getMessage()->shouldBeEqualTo('Value for "dummy" at path "/level1/level2" not found.');
    }

    public function it_prepends_path_to_message(): void
    {
        $this->setName('dummy');
        $this->prependPath('level1');
        $this->getMessage()->shouldBeEqualTo('Value for "dummy" at path "/level1" not found.');
        $this->prependPath('level2');
        $this->getMessage()->shouldBeEqualTo('Value for "dummy" at path "/level2/level1" not found.');
    }
}
