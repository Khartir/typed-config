<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Exception;

use Khartir\TypedConfig\Exception\AbstractException;
use PhpSpec\ObjectBehavior;

class InvalidArgumentExceptionSpec extends ObjectBehavior
{
    public function it_extends_base_exception(): void
    {
        $this->shouldHaveType(AbstractException::class);
    }

    public function it_adds_name_to_message(): void
    {
        $this::create('Invalid value "3" expected value of type "int"', 'dummy')
            ->getMessage()->shouldBeEqualTo('Invalid value "3" expected value of type "int" for "dummy" at path "/".');
    }

    public function it_adds_path_to_message(): void
    {
        $test = $this::create('Invalid value "3" expected value of type "int"', 'dummy');
        $test->addPath('level1');
        $test->getMessage()->shouldBeEqualTo(
            'Invalid value "3" expected value of type "int" for "dummy" at path "/level1".'
        );
        $test->addPath('level2');
        $test->getMessage()->shouldBeEqualTo(
            'Invalid value "3" expected value of type "int" for "dummy" at path "/level1/level2".'
        );
    }

    public function it_prepends_path_to_message(): void
    {
        $test = $this::create('Invalid value "3" expected value of type "int"', 'dummy');
        $test->prependPath('level1');
        $test->getMessage()->shouldBeEqualTo(
            'Invalid value "3" expected value of type "int" for "dummy" at path "/level1".'
        );
        $test->prependPath('level2');
        $test->getMessage()->shouldBeEqualTo(
            'Invalid value "3" expected value of type "int" for "dummy" at path "/level2/level1".'
        );
    }
}
