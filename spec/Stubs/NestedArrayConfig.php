<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class NestedArrayConfig
{
    /**
     * @var ArrayConfig
     */
    public $nested;

    public function __construct(ArrayConfig $nested)
    {
        $this->nested = $nested;
    }
}
