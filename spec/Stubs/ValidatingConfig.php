<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

use Khartir\TypedConfig\Exception\InvalidArgumentException;

class ValidatingConfig
{
    public function __construct(string $test)
    {
        throw InvalidArgumentException::create('Invalid Argument', 'test');
    }
}
