<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class UnconstructableConfig
{
    private function __construct()
    {
    }
}
