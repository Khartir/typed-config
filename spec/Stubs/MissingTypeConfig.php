<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class MissingTypeConfig
{
    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingAnyTypeHint
     */
    public function __construct($test)
    {
    }
}
