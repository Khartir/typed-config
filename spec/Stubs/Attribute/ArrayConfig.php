<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs\Attribute;

//phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.UnusedUse
use Khartir\TypedConfig\Resolver\ScalarTypeResolver;
use Khartir\TypedConfig\Attribute\Resolve;
use Khartir\TypedConfig\Resolver\ArrayOverwriteResolver;

class ArrayConfig
{
    /**
     * @var array<mixed>
     */
    public $values;

    /**
     * @param array<mixed> $values
     */
    public function __construct(
        //phpcs:disable Squiz.Functions.MultiLineFunctionDeclaration.FirstParamSpacing
        #[Resolve([ScalarTypeResolver::class, ArrayOverwriteResolver::class])]
        array $values
    ) {
        $this->values = $values;
    }
}
