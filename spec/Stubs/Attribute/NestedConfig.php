<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs\Attribute;

class NestedConfig
{
    /**
     * @var ArrayConfig
     */
    public $child;

    /**
     * @var bool
     */
    public $test;

    public function __construct(ArrayConfig $child, bool $test)
    {
        $this->child = $child;
        $this->test  = $test;
    }
}
