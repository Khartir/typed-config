<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class SimpleConfig
{

    /**
     * @var string
     */
    public $foo;

    /**
     * @var int
     */
    public $count;

    public function __construct(string $foo, int $count)
    {
        $this->foo = $foo;
        $this->count = $count;
    }
}
