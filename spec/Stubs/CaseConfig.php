<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class CaseConfig
{
    /**
     * @var string
     */
    public $fooBar;

    public function __construct(string $fooBar)
    {
        $this->fooBar = $fooBar;
    }
}
