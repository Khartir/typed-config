<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class MissingNestedClassConfig
{
    public function __construct(UnknownClass $test)
    {
    }
}
