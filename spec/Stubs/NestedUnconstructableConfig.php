<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class NestedUnconstructableConfig
{
    public function __construct(UnconstructableConfig $test)
    {
    }
}
