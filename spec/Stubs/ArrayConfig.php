<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class ArrayConfig
{
    /**
     * @var array<mixed>
     */
    public $values;

    /**
     * @param array<mixed> $values
     */
    public function __construct(array $values)
    {
        $this->values = $values;
    }
}
