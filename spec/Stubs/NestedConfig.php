<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class NestedConfig
{

    /**
     * @var SimpleConfig
     */
    public $simple;

    /**
     * @var bool
     */
    public $test;

    public function __construct(SimpleConfig $simple, bool $test)
    {
        $this->simple = $simple;
        $this->test = $test;
    }
}
