<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs\Annotation;

class ArrayConfig
{
    /**
     * @var array<mixed>
     */
    public $values;

    /**
     * @param array<mixed> $values
     * @\Khartir\TypedConfig\Annotation\Resolve({
     *   "values": {
     *     \Khartir\TypedConfig\Resolver\ScalarTypeResolver::class,
     *     \Khartir\TypedConfig\Resolver\ArrayOverwriteResolver::class,
     *   }
     * })
     */
    public function __construct(array $values)
    {
        $this->values = $values;
    }
}
