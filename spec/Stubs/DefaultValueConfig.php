<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class DefaultValueConfig
{

    /**
     * @var string
     */
    public $foo;

    /**
     * @var int
     */
    public $count;

    /**
     * @var string|null
     */
    public $nullable;

    /**
     * @var array<mixed>
     */
    public $array;

    /**
     * @param array<mixed> $array
     */
    public function __construct(string $foo = 'default', int $count = 99, ?string $nullable = '', array $array = [])
    {
        $this->foo = $foo;
        $this->count = $count;
        $this->nullable = $nullable;
        $this->array = $array;
    }
}
