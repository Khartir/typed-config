<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class AllScalarConfig
{

    /**
     * @var string
     */
    public $foo;

    /**
     * @var int
     */
    public $count;

    /**
     * @var bool
     */
    public $test;

    /**
     * @var float
     */
    public $percentage;

    public function __construct(string $foo, int $count, bool $test, float $percentage)
    {
        $this->foo = $foo;
        $this->count = $count;
        $this->test = $test;
        $this->percentage = $percentage;
    }
}
