<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Stubs;

class OptionalNested
{
    /**
     * @var string
     */
    public $foo;

    /**
     * @var ?SimpleConfig
     */
    public $nested;

    /**
     * @var ?SimpleConfig
     */
    public $otherNested;

    public function __construct(string $foo, ?SimpleConfig $nested = null, ?SimpleConfig $otherNested = null)
    {
        $this->foo = $foo;
        $this->nested = $nested;
        $this->otherNested = $otherNested;
    }
}
