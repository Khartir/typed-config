<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig;

use Khartir\TypedConfig\Extractor\SnakeCaseExtractor;
use Khartir\TypedConfig\Resolver\ScalarTypeResolver;
use PhpSpec\Exception\Example\SkippingException;
use PhpSpec\ObjectBehavior;
use spec\Khartir\TypedConfig\Stubs\AllScalarConfig;
use spec\Khartir\TypedConfig\Stubs\ArrayConfig;
use spec\Khartir\TypedConfig\Stubs\CaseConfig;
use spec\Khartir\TypedConfig\Stubs\DefaultValueConfig;
use spec\Khartir\TypedConfig\Stubs\NestedConfig;
use spec\Khartir\TypedConfig\Stubs\OptionalNested;
use spec\Khartir\TypedConfig\Stubs\SimpleConfig;

class DummyValueBuilderSpec extends ObjectBehavior
{
    public function it_instantiates_a_simple_object_with_dummy_values(): void
    {
        $this->build(SimpleConfig::class, [])
            ->shouldBeLike(new SimpleConfig('', 0));
    }

    public function it_instantiates_an_object_with_a_nested_object_with_dummy_values(): void
    {
        $this->build(NestedConfig::class, [])
            ->shouldBeLike(new NestedConfig(new SimpleConfig('', 0), false));
    }

    public function it_can_handle_array_parameters_with_dummy_values(): void
    {
        $this->build(
            ArrayConfig::class,
            [
            ]
        )->shouldBeLike(new ArrayConfig([]));
    }

    public function it_uses_matching_values_if_given(): void
    {
        $this->beConstructedWith(null, [new ScalarTypeResolver()]);
        $this->build(SimpleConfig::class, [['count' => 3]])
            ->shouldBeLike(new SimpleConfig('', 3));
    }

    public function it_sets_defaults_with_annotations(): void
    {
        $this->enableAnnotations();
        $this->build(Stubs\Annotation\ArrayConfig::class, [])
            ->shouldBeLike(new Stubs\Annotation\ArrayConfig([]));
    }

    public function it_sets_defaults_with_attributes(): void
    {
        if (version_compare(PHP_VERSION, '8.0', '<')) {
            throw new SkippingException('Requires PHP 8');
        }
        $this->build(Stubs\Attribute\NestedConfig::class, [])
            ->shouldBeLike(
                new Stubs\Attribute\NestedConfig(
                    new Stubs\Attribute\ArrayConfig([]),
                    false
                )
            );
    }

    public function it_does_not_resolve_to_null_if_configured(): void
    {
        $this->useNullable(false);
        $this->build(DefaultValueConfig::class, [])
            ->shouldBeLike(new DefaultValueConfig());
    }

    public function it_does_not_resolve_to_default_values_if_configured(): void
    {
        $this->useDefaultValue(false);
        $this->addDefaultValues(['array' => ['foo' => 'bar'],]);
        $this->build(DefaultValueConfig::class, [])
            ->shouldBeLike(new DefaultValueConfig('', 0, null, ['foo' => 'bar']));
    }

    public function it_uses_given_extractor(): void
    {
        $this->beConstructedWith(new SnakeCaseExtractor());
        $this->build(CaseConfig::class, [['foo_bar' => 'test']])
            ->shouldBeLike(new CaseConfig('test'));
    }

    public function it_allows_overriding_default_values(): void
    {
        $this->setDefaultValues(
            [
                'int' => 3,
                'string' => 'dummy',
                'float' => 1.5,
                'bool' => true,
            ]
        );
        $this->build(AllScalarConfig::class, [])
            ->shouldBeLike(new AllScalarConfig('dummy', 3, true, 1.5));
    }

    public function it_allows_optional_nested_configs(): void
    {
        $this->build(OptionalNested::class, [])
            ->shouldBeLike(new OptionalNested(''));
    }
}
