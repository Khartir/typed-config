<?php declare(strict_types = 1);

namespace spec\Khartir\TypedConfig\Attribute;

use PhpSpec\Exception\Example\SkippingException;
use PhpSpec\ObjectBehavior;

class ResolveSpec extends ObjectBehavior
{
    public function it_validates_resolver_names(): void
    {
        if (version_compare(PHP_VERSION, '8.0', '<')) {
            throw new SkippingException('Requires PHP 8');
        }
        $this->beConstructedWith('dummy');
        $this->shouldThrow(new \InvalidArgumentException(
            'Resolver must implement Khartir\TypedConfig\Resolver\ResolverInterface, dummy given'
        ))
            ->duringInstantiation();
    }
}
