<?php declare(strict_types = 1);

namespace Khartir\TypedConfig;

class ReflectionHelper
{
    public static function getTypeName(\ReflectionParameter $parameter): ?string
    {
        $type = $parameter->getType();
        if ($type === null) {
            return null;
        }

        if ($type instanceof \ReflectionNamedType) {
            return $type->getName();
        }

        return (string)$type;
    }
}
