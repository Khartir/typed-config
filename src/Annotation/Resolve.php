<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Annotation;

use Khartir\TypedConfig\Resolver\ResolverInterface;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Resolve
{
    /**
     * @var array<string, array<class-string<ResolverInterface>>>
     */
    private $map;

    /**
     * @param array{"value": array<string, class-string<ResolverInterface>|array<class-string<ResolverInterface>>>} $map
     */
    public function __construct(array $map)
    {
        $validMap = [];
        foreach ($map['value'] as $name => $resolvers) {
            if (!is_array($resolvers)) {
                $resolvers = [$resolvers];
            }
            foreach ($resolvers as $resolver) {
                /** @phpstan-ignore-next-line https://github.com/phpstan/phpstan-strict-rules/issues/120 */
                if (!is_subclass_of($resolver, ResolverInterface::class)) {
                    throw new \InvalidArgumentException(\sprintf(
                        'Resolver must implement %s, %s given',
                        ResolverInterface::class,
                        $resolver
                    ));
                }
            }
            $validMap[$name] = $resolvers;
        }

        $this->map = $validMap;
    }

    public function hasResolvers(string $name): bool
    {
        return isset($this->map[$name]);
    }

    /**
     * @return array<ResolverInterface>
     */
    public function getResolvers(string $name): array
    {
        $result = [];
        foreach ($this->map[$name] as $resolver) {
            $result[] = new $resolver;
        }

        return $result;
    }
}
