<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Exception;

class MissingValueException extends AbstractParameterException
{
    private const TEMPLATE = 'Value for "%s" at path "/%s" not found.';

    public static function create(string $name): self
    {
        $exception = new MissingValueException();
        $exception->setName($name);

        return $exception;
    }

    protected function setMessage(): void
    {
        $this->message = \sprintf(self::TEMPLATE, $this->name, \implode('/', $this->path));
    }
}
