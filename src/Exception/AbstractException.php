<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Exception;

/**
 * The base exception of this project. All other exceptions extend this.
 */
abstract class AbstractException extends \Exception
{

}
