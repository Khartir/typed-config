<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Exception;

class InvalidRootClassException extends AbstractException
{

}
