<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Exception;

abstract class AbstractParameterException extends AbstractException
{
    /**
     * @var array<string>
     */
    protected $path = [];

    /**
     * @var string
     */
    protected $name;

    public function setName(string $name): void
    {
        $this->name = $name;
        $this->setMessage();
    }

    public function addPath(string $path): void
    {
        $this->path[] = $path;
        $this->setMessage();
    }

    public function prependPath(string $path): void
    {
        array_unshift($this->path, $path);
        $this->setMessage();
    }

    abstract protected function setMessage(): void;
}
