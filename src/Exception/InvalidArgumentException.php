<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Exception;

use Khartir\TypedConfig\ReflectionHelper;

class InvalidArgumentException extends AbstractParameterException
{
    /**
     * @var string
     */
    private $partialMessage;

    public static function create(string $partialMessage, string $name): self
    {
        $exception = new InvalidArgumentException();
        $exception->partialMessage = $partialMessage;
        $exception->setName($name);

        return $exception;
    }

    /**
     * @param mixed $value
     */
    public static function createForParameter($value, \ReflectionParameter $parameter): self
    {
        switch (gettype($value)) {
            case 'NULL':
                $value = 'NULL';
                break;
            case 'boolean':
                $value = $value === true ? 'TRUE' : 'FALSE';
                break;
            case 'double':
            case 'integer':
                break;
            case 'string':
                $value = sprintf('"%s"', $value);
                break;
            default:
                $value = sprintf('of type "%s"', gettype($value));
                break;
        }

        $expectedType = ReflectionHelper::getTypeName($parameter);
        if ($parameter->allowsNull()) {
            $expectedType .= ' or NULL';
        }

        $message = sprintf('Invalid value %s, expected value of type %s', $value, $expectedType);

        return self::create($message, $parameter->getName());
    }

    protected function setMessage(): void
    {
        $this->message = \sprintf(
            '%s for "%s" at path "/%s".',
            $this->partialMessage,
            $this->name,
            \implode('/', $this->path)
        );
    }
}
