<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Exception;

class InvalidPropertyException extends AbstractParameterException
{

    /**
     * @var string
     */
    private $partialMessage;

    public static function create(string $partialMessage, string $name): self
    {
        $exception = new InvalidPropertyException();
        $exception->partialMessage = $partialMessage;
        $exception->setName($name);

        return $exception;
    }

    protected function setMessage(): void
    {
        $this->message = \sprintf(
            '%s for "%s" at path "/%s".',
            $this->partialMessage,
            $this->name,
            \implode('/', $this->path)
        );
    }
}
