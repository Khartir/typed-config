<?php declare(strict_types = 1);

namespace Khartir\TypedConfig;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\Reader;
use Khartir\TypedConfig\Attribute\Resolve;
use Khartir\TypedConfig\Exception\AbstractParameterException;
use Khartir\TypedConfig\Exception\InvalidArgumentException;
use Khartir\TypedConfig\Exception\InvalidPropertyException;
use Khartir\TypedConfig\Exception\InvalidRootClassException;
use Khartir\TypedConfig\Exception\MissingValueException;
use Khartir\TypedConfig\Extractor\ExactMatchExtractor;
use Khartir\TypedConfig\Extractor\ExtractorInterface;
use Khartir\TypedConfig\Resolver\ArrayMergeResolver;
//phpcs:ignore
use Khartir\TypedConfig\Resolver\ResolverInterface;
use Khartir\TypedConfig\Resolver\ScalarTypeResolver;
use ReflectionClass;
use ReflectionException;

class Builder
{
    /**
     * @var ExtractorInterface
     */
    private $extractor;

    /**
     * @var array<ResolverInterface>
     */
    private $resolvers;

    /**
     * @var Reader
     */
    private $annotationReader;

    /**
     * @param array<ResolverInterface>|null $resolver
     */
    public function __construct(?ExtractorInterface $extractor = null, ?array $resolver = null)
    {
        $this->extractor = $extractor ?? new ExactMatchExtractor();
        if ($resolver !== null) {
            foreach ($resolver as $singleResolver) {
                if (!$singleResolver instanceof ResolverInterface) {
                    throw new \InvalidArgumentException(
                        'Resolver must implement ' . ResolverInterface::class
                    );
                }
            }
        }
        $this->resolvers = $resolver ?? [
                new ArrayMergeResolver(),
                new ScalarTypeResolver(),
            ];
    }

    /**
     * @template T of object
     * @param class-string<T> $className
     * @param array<array<string, mixed>> $data
     * @return T
     */
    public function build(string $className, array $data): object
    {
        return $this->buildWithPath($className, $data, null);
    }

    public function enableAnnotations(?Reader $annotationReader = null): void
    {
        if ($annotationReader === null) {
            $annotationReader = new AnnotationReader();
        }
        $this->annotationReader = $annotationReader;
    }

    /**
     * @template T of object
     * @param class-string<T> $className
     * @param array<array<string, mixed>> $data
     * @return T
     */
    private function buildWithPath(string $className, array $data, ?string $path): object
    {
        try {
            $class = new ReflectionClass($className);
        } catch (ReflectionException $exception) {
            throw new InvalidRootClassException(sprintf('Root-class "%s" not found.', $className));
        }

        if ($class->getConstructor() === null) {
            if ($path === null) {
                throw new InvalidRootClassException(
                    sprintf('Root-class "%s" does not have a constructor.', $class->getName())
                );
            }

            return $class->newInstance();
        }

        if (!$class->getConstructor()->isPublic()) {
            if ($path === null) {
                throw new InvalidRootClassException(
                    sprintf('Root-class "%s" does not have a public constructor.', $class->getName())
                );
            }

            throw InvalidPropertyException::create(
                sprintf('Class "%s" does not have a public constructor', $class->getName()),
                $path
            );
        }

        foreach ($data as $key => $list) {
            if (!is_array($list)) {
                throw InvalidArgumentException::create(
                    \sprintf('List of arrays expected, %s given at key %s', gettype($list), $key),
                    $path ?? 'root-element'
                );
            }
        }


        try {
            return $this->instantiateClassWithValidConstructor($class, $data);
        } catch (AbstractParameterException $e) {
            if ($path !== null) {
                $e->addPath($path);
            }
            throw $e;
        }
    }

    /**
     * @template T of object
     * @param ReflectionClass<T> $class
     * @param array<array<string, mixed>> $data
     * @return T
     * @throws ReflectionException
     * @throws \Throwable
     */
    private function instantiateClassWithValidConstructor(
        ReflectionClass $class,
        array $data
    ): object {
        $args = [];
        $constructor = $class->getConstructor();
        assert($constructor !== null);

        foreach ($constructor->getParameters() as $parameter) {
            if ($this->resolve($parameter, $data, $args)) {
                continue;
            }
            $type = $parameter->getType();
            if ($type === null) {
                throw InvalidPropertyException::create('Missing parameter type', $parameter->getName());
            }
            if (method_exists($type, 'isBuiltin') && $type->isBuiltin()) {
                throw InvalidPropertyException::create(
                    sprintf(
                        'Invalid type %s',
                        ReflectionHelper::getTypeName($parameter)
                    ),
                    $parameter->getName()
                );
            }

            $className = ReflectionHelper::getTypeName($parameter);
            \assert($className !== null);
            if (!class_exists($className)) {
                throw InvalidPropertyException::create(
                    sprintf('Class "%s" not found', $className),
                    $parameter->getName()
                );
            }
            if ($parameter->isDefaultValueAvailable()) {
                $values = $this->extractor->findOptionalValues($parameter->getName(), $data);
                if ($values === null) {
                    $args[] = $parameter->getDefaultValue();
                    continue;
                }
            } else {
                $values = $this->extractor->findValues($parameter->getName(), $data);
            }
            $args[] = $this->buildWithPath(
                $className,
                $values,
                $parameter->getName()
            );
        }

        return $class->newInstanceArgs($args);
    }

    /**
     * @param array<array<string, mixed>> $data
     * @param array<mixed> $args
     */
    private function resolve(\ReflectionParameter $parameter, array $data, array &$args): bool
    {

        foreach ($this->getResolvers($parameter) as $resolver) {
            if ($resolver->canResolve($parameter)) {
                try {
                    $args[] = $resolver->resolve(
                        $parameter,
                        $this->extractor->findValues($parameter->getName(), $data)
                    );
                } catch (MissingValueException $e) {
                    if (!$parameter->isDefaultValueAvailable()) {
                        throw $e;
                    }

                    $args[] = $parameter->getDefaultValue();
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @return array<ResolverInterface>
     */
    protected function getResolvers(\ReflectionParameter $parameter): array
    {
        $resolvers = $this->resolvers;
        if (method_exists($parameter, 'getAttributes')) {
            $attributes = $parameter->getAttributes(Resolve::class);
            $attribute = array_shift($attributes);
            if ($attribute !== null) {
                /** @var Resolve $attributeInstance */
                $attributeInstance = $attribute->newInstance();
                $resolvers = $attributeInstance->getResolvers();
            }
        }
        if ($this->annotationReader !== null) {
            /** @var \ReflectionMethod $method */
            $method = $parameter->getDeclaringFunction();
            /** @var ?Annotation\Resolve $annotation */
            $annotation = $this->annotationReader->getMethodAnnotation(
                $method,
                Annotation\Resolve::class
            );
            if ($annotation !== null && $annotation->hasResolvers($parameter->getName())) {
                $resolvers = $annotation->getResolvers($parameter->getName());
            }
        }

        return $resolvers;
    }
}
