<?php declare(strict_types = 1);

namespace Khartir\TypedConfig;

use Khartir\TypedConfig\Extractor\DummyValueExtractor;
use Khartir\TypedConfig\Extractor\ExactMatchExtractor;
use Khartir\TypedConfig\Extractor\ExtractorInterface;
use Khartir\TypedConfig\Resolver\DummyValueResolver;
// phpcs:ignore SlevomatCodingStandard.Namespaces.UnusedUses.UnusedUse
use Khartir\TypedConfig\Resolver\ResolverInterface;

class DummyValueBuilder extends Builder
{
    /**
     * @var bool
     */
    private $preferNullable = true;

    /**
     * @var bool
     */
    private $preferDefault = true;

    /**
     * @var array<string, mixed>
     */
    private $defaultValues = [
        'int' => 0,
        'string' => '',
        'float' => .0,
        'array' => [],
        'bool' => false,
    ];

    /**
     * @param array<ResolverInterface>|null $resolvers
     */
    public function __construct(?ExtractorInterface $extractor = null, ?array $resolvers = null)
    {
        parent::__construct(new DummyValueExtractor($extractor ?? new ExactMatchExtractor()), $resolvers);
    }

    /**
     * @inheritdoc
     */
    protected function getResolvers(\ReflectionParameter $parameter): array
    {
        return [new DummyValueResolver(
            parent::getResolvers($parameter),
            $this->preferDefault,
            $this->preferNullable,
            $this->defaultValues
        )];
    }

    /**
     * @param bool $flag if true uses null on nullable parameters instead of default value
     */
    public function useNullable(bool $flag): self
    {
        $this->preferNullable = $flag;

        return $this;
    }

    /**
     * @param bool $flag if true uses a parameters default value if possible
     */
    public function useDefaultValue(bool $flag): self
    {
        $this->preferDefault = $flag;

        return $this;
    }

    /**
     * @param array<string, mixed> $defaultValues
     */
    public function setDefaultValues(array $defaultValues): void
    {
        $this->defaultValues = $defaultValues;
    }

    /**
     * @param array<string, mixed> $defaultValues
     */
    public function addDefaultValues(array $defaultValues): void
    {
        $this->defaultValues = \array_merge($this->defaultValues, $defaultValues);
    }
}
