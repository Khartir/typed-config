<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Resolver;

use Khartir\TypedConfig\Exception\InvalidArgumentException;
use Khartir\TypedConfig\ReflectionHelper;

class ArrayOverwriteResolver implements ResolverInterface
{
    public function canResolve(\ReflectionParameter $parameter): bool
    {
        return ReflectionHelper::getTypeName($parameter) === 'array';
    }

    /**
     * @param array<mixed> $values
     * @return array<mixed>|null
     */
    public function resolve(\ReflectionParameter $parameter, array $values): ?array
    {
        $value = \end($values);
        if (!$this->validateType($value, $parameter)) {
            $exception = InvalidArgumentException::createForParameter(
                $value,
                $parameter
            );
            throw $exception;
        }

        return $value;
    }

    /**
     * @param mixed $value
     */
    private function validateType($value, \ReflectionParameter $parameter): bool
    {
        if ($value === null && $parameter->allowsNull()) {
            return true;
        }

        return \is_array($value);
    }
}
