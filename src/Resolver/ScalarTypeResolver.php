<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Resolver;

use Khartir\TypedConfig\Exception\InvalidArgumentException;
use Khartir\TypedConfig\ReflectionHelper;

class ScalarTypeResolver implements ResolverInterface
{
    public function canResolve(\ReflectionParameter $parameter): bool
    {
        return in_array(ReflectionHelper::getTypeName($parameter), ['int', 'string', 'bool', 'float'], true);
    }

    /**
     * @param array<mixed> $values
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function resolve(\ReflectionParameter $parameter, array $values)
    {
        $value = \end($values);
        if (!$this->validateType($value, $parameter)) {
            $exception = InvalidArgumentException::createForParameter(
                $value,
                $parameter
            );
            throw $exception;
        }

        return $value;
    }

    /**
     * @param mixed $value
     */
    private function validateType($value, \ReflectionParameter $parameter): bool
    {
        switch (\gettype($value)) {
            case 'integer':
                $type = 'int';
                break;
            case 'boolean':
                $type = 'bool';
                break;
            case 'double':
                $type = 'float';
                break;
            default:
                $type = \gettype($value);
        }
        if ($value === null && $parameter->allowsNull()) {
            return true;
        }

        return $type === ReflectionHelper::getTypeName($parameter);
    }
}
