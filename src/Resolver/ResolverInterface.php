<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Resolver;

interface ResolverInterface
{
    public function canResolve(\ReflectionParameter $parameter): bool;

    /**
     * @param array<mixed> $values
     * @return mixed
     */
    public function resolve(\ReflectionParameter $parameter, array $values);
}
