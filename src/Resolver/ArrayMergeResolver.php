<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Resolver;

use Khartir\TypedConfig\Exception\InvalidArgumentException;
use Khartir\TypedConfig\ReflectionHelper;

class ArrayMergeResolver implements ResolverInterface
{
    public function canResolve(\ReflectionParameter $parameter): bool
    {
        return ReflectionHelper::getTypeName($parameter) === 'array';
    }

    /**
     * @param array<mixed> $values
     * @return null|array<mixed>
     */
    public function resolve(\ReflectionParameter $parameter, array $values): ?array
    {
        if ($parameter->allowsNull()) {
            $values = array_filter($values, function ($value): bool {
                return $value !== null;
            });
            if (count($values) === 0) {
                return null;
            }
        }
        foreach ($values as $value) {
            if (!is_array($value)) {
                throw InvalidArgumentException::createForParameter(
                    $value,
                    $parameter
                );
            }
        }
        return array_merge(...$values);
    }
}
