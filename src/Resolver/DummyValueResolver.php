<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Resolver;

use Khartir\TypedConfig\ReflectionHelper;

class DummyValueResolver implements ResolverInterface
{
    /**
     * @var bool
     */
    private $preferNullable;

    /**
     * @var bool
     */
    private $preferDefault;

    /**
     * @var array<string, mixed>
     */
    private $defaults;

    /**
     * @var array<ResolverInterface>
     */
    private $decoratedResolvers;

    /**
     * @param array<ResolverInterface> $resolvers
     * @param array<string, mixed> $defaults
     */
    public function __construct(
        array $resolvers,
        bool $preferDefault,
        bool $preferNullable,
        array $defaults
    ) {
        $this->preferNullable = $preferNullable;
        $this->preferDefault  = $preferDefault;
        $this->decoratedResolvers = $resolvers;
        $this->defaults = $defaults;
    }

    public function canResolve(\ReflectionParameter $parameter): bool
    {
        return in_array(ReflectionHelper::getTypeName($parameter), \array_keys($this->defaults), true);
    }

    /**
     * @param array<mixed> $values
     * @return mixed
     */
    public function resolve(\ReflectionParameter $parameter, array $values)
    {
        if ($values !== []) {
            foreach ($this->decoratedResolvers as $resolver) {
                if ($resolver->canResolve($parameter)) {
                    return $resolver->resolve($parameter, $values);
                }
            }
        }
        if ($this->preferDefault && $parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }

        if ($this->preferNullable && $parameter->allowsNull()) {
            return null;
        }

        return $this->defaults[ReflectionHelper::getTypeName($parameter)];
    }
}
