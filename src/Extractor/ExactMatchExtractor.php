<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Extractor;

use Khartir\TypedConfig\Exception\MissingValueException;

class ExactMatchExtractor implements ExtractorInterface
{
    /**
     * @param array<array<string, mixed>> $data
     * @return array<mixed>
     * @throws MissingValueException
     */
    public function findValues(string $key, array $data): array
    {
        $result = $this->findOptionalValues($key, $data);

        if ($result === null) {
            throw MissingValueException::create($key);
        }

        return $result;
    }

    /**
     * @param array<array<string, mixed>> $data
     * @return ?array<mixed>
     */
    public function findOptionalValues(string $key, array $data): ?array
    {
        $result = [];
        foreach ($data as $values) {
            if (!\array_key_exists($key, $values)) {
                continue;
            }

            $result[] = $values[$key];
        }

        if (\count($result) === 0) {
            return null;
        }

        return $result;
    }
}
