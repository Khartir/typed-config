<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Extractor;

//phpcs:ignore
use Khartir\TypedConfig\Exception\MissingValueException;

interface ExtractorInterface
{
    /**
     * @param array<array<string, mixed>> $data
     * @return array<mixed>
     * @throws MissingValueException
     */
    public function findValues(string $key, array $data): array;

    /**
     * @param array<array<string, mixed>> $data
     * @return ?array<mixed>
     */
    public function findOptionalValues(string $key, array $data): ?array;
}
