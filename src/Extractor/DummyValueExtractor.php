<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Extractor;

class DummyValueExtractor implements ExtractorInterface
{
    /**
     * @var ?ExtractorInterface
     */
    private $decorated;

    public function __construct(?ExtractorInterface $decorated = null)
    {
        $this->decorated = $decorated;
    }

    /**
     * @param array<array<string, mixed>> $data
     * @return array<mixed>
     */
    public function findValues(string $key, array $data): array
    {
        if ($this->decorated !== null) {
            $values = $this->decorated->findOptionalValues($key, $data);
            if ($values === null) {
                $values = [];
            }

            return $values;
        }
        return [];
    }

    /**
     * @param array<array<string, mixed>> $data
     * @return ?array<mixed>
     */
    public function findOptionalValues(string $key, array $data): ?array
    {
        if ($this->decorated !== null) {
            return $this->decorated->findOptionalValues($key, $data);
        }

        return null;
    }
}
