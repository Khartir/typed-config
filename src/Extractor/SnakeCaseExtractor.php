<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Extractor;

use Khartir\TypedConfig\Exception\InvalidPropertyException;
//phpcs:ignore
use Khartir\TypedConfig\Exception\MissingValueException;

class SnakeCaseExtractor extends ExactMatchExtractor
{
    /**
     * @param array<array<string, mixed>> $data
     * @return array<mixed>
     * @throws MissingValueException
     * @throws InvalidPropertyException
     */
    public function findValues(string $key, array $data): array
    {
        $key = $this->convertKey($key);
        return parent::findValues($key, $data);
    }

    /**
     * @param array<array<string, mixed>> $data
     * @return ?array<mixed>
     */
    public function findOptionalValues(string $key, array $data): ?array
    {
        $key = $this->convertKey($key);
        return parent::findOptionalValues($key, $data);
    }

    /**
     * @throws InvalidPropertyException
     */
    private function convertKey(string $key): string
    {
        $formatted = preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $key);
        if ($formatted === null) {
            throw InvalidPropertyException::create(sprintf(
                'Could not convert property to snake_case (preg error code: %s)',
                preg_last_error()
            ), $key);
        }
        return strtolower($formatted);
    }
}
