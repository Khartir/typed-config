<?php declare(strict_types = 1);

namespace Khartir\TypedConfig\Attribute;

use Khartir\TypedConfig\Resolver\ResolverInterface;

#[\Attribute(\Attribute::TARGET_PARAMETER)]
class Resolve
{
    /**
     * @var array<class-string<ResolverInterface>>
     */
    private $resolvers;

    /**
     * @param class-string<ResolverInterface>|array<class-string<ResolverInterface>> $resolvers
     */
    public function __construct($resolvers)
    {
        if (!is_array($resolvers)) {
            $resolvers = [$resolvers];
        }
        foreach ($resolvers as $resolver) {
            if (!is_subclass_of($resolver, ResolverInterface::class)) {
                throw new \InvalidArgumentException(\sprintf(
                    'Resolver must implement %s, %s given',
                    ResolverInterface::class,
                    $resolver
                ));
            }
        }
        $this->resolvers = $resolvers;
    }

    /**
     * @return array<ResolverInterface>
     */
    public function getResolvers(): array
    {
        $result = [];
        foreach ($this->resolvers as $resolver) {
            $result[] = new $resolver;
        }

        return $result;
    }
}
