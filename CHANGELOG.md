# Change Log

## 0.3

**Added:**
- Nested configs can now be nullable and have a default value of `null`.

**BC-Breaking changes:**
- `ExtractorInterface` gained a new method `findOptionalValues`.

## 0.2.1

**Added:**

- `DummyValueBuilder` for tests. See readme for mor information.
- Resolvers are now validated on `Builder` construction.
